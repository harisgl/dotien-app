import Image from "next/image";
import Layout from "@/components/Layout";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import styles from "./page.module.css";
import CardAction from "@/components/CardAction";
import InputSearch from "@/components/InputSearch";

interface BreedsApiResponse {
  message: {
    [breed: string]: string[];
  };
  status: string;
}

const getData = async (): Promise<string[]> => {
  try {
    const response = await fetch("https://dog.ceo/api/breeds/list/all");
    if (!response.ok) {
      throw new Error(`Error fetching data: ${response.statusText}`);
    }
    const data: BreedsApiResponse = await response.json();

    if (data.status !== "success") {
      throw new Error("Failed to fetch breeds");
    }

    const breeds =
      data.message && typeof data.message === "object"
        ? Object.entries(data.message).reduce((acc, [breed, subBreeds]) => {
            if (subBreeds.length) {
              subBreeds.forEach((subBreed) => {
                acc.push(`${subBreed} ${breed}`);
              });
            } else {
              acc.push(breed);
            }
            return acc;
          }, [] as string[])
        : [];

    return breeds;
  } catch (error) {
    console.error("Failed to fetch breeds:", error);
    return [];
  }
};

const Home = async () => {
  const breeds = await getData();
  return (
    <Layout>
      <Box
        className={styles.thumbnailWrapper}
        sx={{ margin: { xs: "-16px", md: "-32px" } }}
      >
        <Image
          src={"/images/thumbnail.png"}
          alt="Thumbnail"
          priority
          style={{ objectFit: "contain", width: "100%", height: "auto" }}
          width={2788}
          height={1724}
        />
      </Box>
      <Box
        zIndex={3}
        position="relative"
        display="flex"
        justifyContent="center"
      >
        <Box
          sx={{ minWidth: { xs: 200, md: 400, lg: 600 } }}
          className={styles.searchWrapper}
        >
          <InputSearch />
        </Box>
      </Box>

      <Grid container spacing={2} sx={{ mt: 4 }}>
        {breeds.map((breed, index) => (
          <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
            <Card>
              <CardContent>
                <Typography textTransform={"capitalize"} variant="body1">
                  {breed}
                </Typography>
              </CardContent>
              <CardActions>
                <CardAction breed={breed} />
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Layout>
  );
};

export default Home;
