'use client'
 
import Layout from '@/components/Layout'
import Typography from "@mui/material/Typography";
import { useEffect } from 'react'
 
export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string }
  reset: () => void
}) {
  useEffect(() => {
    console.error(error)
  }, [error])
 
  return (
    <Layout>
      <Typography>Something went wrong while fetching this breed!</Typography>
    </Layout>
  )
}