import Layout from "@/components/Layout";
import Typography from "@mui/material/Typography";
import ImagesGrid from "@/components/ImagesGrid.server";

interface BreedsApiResponse {
  message: string[];
  status: string;
}

const getData = async (slug: string, subslug: string) => {
  const response = await fetch(
    `https://dog.ceo/api/breed/${slug}/${subslug}/images`
  );
  const data: BreedsApiResponse = await response.json();

  const images = data.message;
  return images;
};

const Breed = async ({
  params,
}: {
  params: { slug: string; subslug: string };
}) => {
  const images = await getData(params.slug, params.subslug);
  return (
    <Layout>
      <Typography variant="h4" textTransform={"capitalize"}>
        {params.subslug + " " + params.slug}
      </Typography>
      <ImagesGrid images={images} />
    </Layout>
  );
};

export default Breed;
