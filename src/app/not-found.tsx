import Layout from '@/components/Layout'
 
export default function NotFound() {
  return (
    <Layout>
      <h2>Not Found</h2>
      <p>Could not find requested resource</p>
    </Layout>
  )
}