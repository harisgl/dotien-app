"use client";
import * as React from "react";
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import Search from "@mui/icons-material/Search";
import { toast } from "react-toastify";

export default function InputSearch() {
  const throwErr = () => {
    toast.error("Search feature is currently disabled!");
  };
  return (
    <TextField
      placeholder="Search breeds"
      variant="outlined"
      onClickCapture={throwErr}
      fullWidth
      InputProps={{
        startAdornment: (
          <InputAdornment position="start" sx={{ pl: 1 }}>
            <Search />
          </InputAdornment>
        ),
      }}
    />
  );
}
