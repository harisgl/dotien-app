import * as React from "react";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";

interface Props {
  images: string[];
}
export default function ImagesGrid({ images }: Props) {
  return (
    <Grid container spacing={2} sx={{ mt: 4 }}>
      {images &&
        images.length > 0 &&
        images.map((image, index) => (
          <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
            <Card>
              <CardMedia
                sx={{ height: 300 }}
                image={image}
                title={"dog-" + (index + 1)}
              />
            </Card>
          </Grid>
        ))}
    </Grid>
  );
}
