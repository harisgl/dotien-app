"use client";
import * as React from "react";
import Button from "@mui/material/Button";
import { useRouter } from "next/navigation";

interface Props {
  breed: string;
}

export default function CardAction(props: Props) {
  const router = useRouter();
  const { breed } = props;
  
  const pushRoute = () => {
    const splitSlug = breed.split(" ");
    let slug = "";
    if (splitSlug.length > 1) {
      slug = splitSlug[1] + "/" + splitSlug[0];
    } else {
      slug = breed;
    }
    router.push(`/breeds/${slug}`);
  };
  return (
    <Button size="small" onClick={pushRoute}>
      Learn More
    </Button>
  );
}
