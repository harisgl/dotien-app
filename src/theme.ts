"use client";
import { createTheme } from "@mui/material/styles";
import { Noto_Serif } from "next/font/google";

const noto = Noto_Serif({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
  display: "swap",
});

const theme = createTheme({
  typography: {
    fontFamily: noto.style.fontFamily,
  },
  components: {
    MuiButton: {
      styleOverrides: {
        text: { color: "#000000", fontWeight: "600" },
        root: { borderRadius: "26px" },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          borderRadius: "26px",
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: "26px",
          fontSize: "14px",
          fontWeight: "600",
          padding: "0px 10px",
        },
      },
    },
    MuiCardContent: {
      styleOverrides: {
        root: {
          display: "flex",
          justifyContent: "center",
        },
      },
    },
    MuiCardActions: {
      styleOverrides: {
        root: {
          display: "flex",
          justifyContent: "center",
        },
      },
    },
  },
});

export default theme;
